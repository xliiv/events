from setuptools import find_packages, setup

setup(
    name='events',
    version='0.1',
    description='Timeline of events happening in a company.',
    author='Tymoteusz Jankowski',
    author_email='tymoteusz.jankowski@gmail.com',
    packages=find_packages(exclude=['tests*']),
    install_requires=[
        'django==3.0',
        'djangorestframework==3.11',
        'django-filter==2.2',
    ],
    extras_require={
        'dev': [
            'ipdb',
        ]
    }
)
