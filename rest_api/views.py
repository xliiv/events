from rest_framework import viewsets
from rest_framework import permissions

from django_filters.rest_framework import DjangoFilterBackend
from rest_api.serializers import EventSerializer
from rest_api.models import Event


class EventViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows events to be viewed or edited.
    """
    queryset = Event.objects.all().order_by('-datetime')
    serializer_class = EventSerializer
    http_method_names = ['get', 'post', 'head']
    filterset_fields = ['desc', 'kind', 'datetime']
