from django.db import models
from django.utils import timezone


class Event(models.Model):
    KIND = (
        ('deployment', 'Deployment'),
        ('configuration-change', 'Configuration Change'),
        ('outage', 'Outage'),
    )
    id = models.AutoField(primary_key=True)
    desc = models.CharField(max_length=255)
    kind = models.CharField(max_length=32, choices=KIND)
    datetime = models.DateTimeField(default=timezone.now)
