from rest_framework import serializers

from rest_api.models import Event


class EventSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Event
        fields = ['url', 'desc', 'kind', 'datetime']
