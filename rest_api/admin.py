from django.contrib import admin
from django.contrib.auth.models import Group, User

from rest_api.models import Event


# disable auth FOR admin
class AccessUser(object):
    has_module_perms = has_perm = __getattr__ = lambda s, *a, **kw: True
admin.site.has_permission = lambda r: setattr(r, 'user', AccessUser()) or True


class EventAdmin(admin.ModelAdmin):
    list_display = ('desc', 'kind', 'datetime')

    def has_change_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(Event, EventAdmin)
admin.site.unregister(User)
admin.site.unregister(Group)
