# Generated by Django 3.0.6 on 2020-05-28 14:18

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('desc', models.CharField(max_length=255)),
                ('kind', models.CharField(choices=[('deployment', 'Deployment'), ('configuration-change', 'Configuration Change'), ('outage', 'Outage')], max_length=32)),
                ('datetime', models.DateTimeField()),
            ],
        ),
    ]
