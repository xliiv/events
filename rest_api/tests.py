from datetime import datetime

from django.urls import reverse
from django.utils import timezone
from rest_framework import status
from rest_framework.test import APITestCase

from rest_api.models import Event
from rest_api.serializers import EventSerializer


class TestRestAPIEvent(APITestCase):

    def test_event_created_when_data_provided(self):
        data = {
            'desc': "Some event",
            'kind': 'deployment',
            'datetime': timezone.now(),
        }

        response = self.client.post(
            reverse('event-list'), data, format='json',
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Event.objects.count(), 1)
        self.assertEqual(Event.objects.get().desc, data['desc'])
        self.assertEqual(Event.objects.get().kind, data['kind'])
        self.assertEqual(Event.objects.get().datetime, data['datetime'])

    def test_event_found_when_exists(self):
        event = Event(
            desc="Existing event", kind="deployment", datetime=timezone.now(),
        )
        event.save()

        response = self.client.get(
            reverse('event-detail', args=(event.id,)), format='json',
        )

        found = EventSerializer(data=response.data)
        self.assertTrue(found.is_valid())
        self.assertEqual(event.desc, found.validated_data['desc'])
        self.assertEqual(event.kind, found.validated_data['kind'])
        self.assertEqual(event.datetime, found.validated_data['datetime'])

    def test_event_found_by_event_type(self):
        event = Event(
            desc="Existing event", kind="deployment", datetime=timezone.now(),
        )
        event.save()

        from urllib.parse import urlencode
        response = self.client.get(
            '?'.join([
                reverse('event-list'), urlencode({'kind': event.kind})
            ]),
            format='json',
        )

        self.assertEqual(response.data['results'][0]['kind'], event.kind)
