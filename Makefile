export vpy=.venv/bin/python3

dev-install:
	python3 -m venv .venv
	$(vpy) -m pip install -e .

dev-run: dev-install
	$(vpy) ./manage.py migrate
	$(vpy) ./manage.py createsuperuser --email admin@example.com --username admin --no-input
	$(vpy) ./manage.py runserver

dev-clean:
	rm -rf .venv
	rm db.sqlite3
