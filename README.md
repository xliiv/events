# Events

Timeline of events happening in a company.


# Running

1. Run this command

```shell
make dev-run
```

2. Visit
    1. [http://127.0.0.1:8000/api](http://127.0.0.1:8000/api) to discover available REST API
    2. [http://127.0.0.1:8000/](http://127.0.0.1:8000/) to see event listing


# Potential Future Improvements

* Configure CI
* Better domain investigation and reflect finds in the Event model (now it's very basic)
* Enable [Authetication](https://www.django-rest-framework.org/api-guide/authentication/#authentication)?
  Now, It's disabled even for Django Admin (see rest_api/admin.py)?
* Use Docker?
* Use [Swagger UI](https://www.django-rest-framework.org/topics/documenting-your-api/#third-party-packages)
  for better REST API docs?
* Use async framework?
